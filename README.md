A minimal example for a DNSLink-only authoritative DNS server.

```bash
yarn
node index.js
```

```bash
drill -p 5333 @127.0.0.1 txt _dnslink.sutty.nl
```
